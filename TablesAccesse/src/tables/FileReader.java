package tables;
//������ ����� ��������� ���������� �� txt ������ ����������
//� ��������� �� �� 2 ������� wods � numbers, ����� � ���������� 
//�������� ����� � ������� �������� ������ ������ ����������� � ��������� �������
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileReader {
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public FileReader(String fileName) {
		setFileName(fileName);
		ArrayList<String> wods = new ArrayList<String>();
		ArrayList<Integer> numbers = new ArrayList<Integer>();	
		try {
			List<String> lines = Files.readAllLines(Paths.get(getFileName()), StandardCharsets.UTF_8);

			int x;
			for(int i = 0; i < lines.size()-1; i++) {
				x = lines.get(i).indexOf("=");

				wods.add(lines.get(i).substring(0, x));
				numbers.add(Integer.parseInt(lines.get(i).substring(x+1)));
				System.out.println(wods.get(i));
				System.out.println(numbers.get(i));
			}			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}
